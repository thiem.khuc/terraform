terraform {
  cloud {
    organization = "vietis-thiem"

    workspaces {
      name = "thiemkhuc"
    }
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.25"
    }
  }
}

provider "aws" {
  region = "ap-southeast-1"
}
